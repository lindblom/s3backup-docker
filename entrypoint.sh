#!/bin/bash
set -e

sed -i "s/__KEY__/$KEY/g" /root/.s3cfg
sed -i "s@__SECRET__@$SECRET@g" /root/.s3cfg

exec "$@"
